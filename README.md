![picture](https://i.imgur.com/VvVo5sa.png)

Unofficial Foxtel GO application, Open source and multi-platform for all platforms to use.
Get access to fotel shows, channles and movies all wrapped up into a desktop application!

&nbsp;&nbsp;&nbsp;&nbsp;

![picture](https://i.imgur.com/77tAJ6G.png)

![picture](https://i.imgur.com/wicwFNA.png)


 &nbsp;&nbsp;&nbsp;&nbsp;

  You can install FoxtelGo from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/foxtelgo/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/foxtelgo/application/-/releases)


 ### Author
  * Corey Bruce
